﻿using NHibernate;

namespace UploadFileToS3.Infrastructure.SessionStorage
{
    public interface ISessionStorageContainer
    {
        ISession GetCurrentSession();
        void Store(ISession session);
        void Clear();
    }
}
