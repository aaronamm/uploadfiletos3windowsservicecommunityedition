﻿using System.Web;

namespace UploadFileToS3.Infrastructure.SessionStorage
{
    public static class SessionStorageFactory
    {
        public static ISessionStorageContainer sessionStorageContainer;

        public static ISessionStorageContainer GetStorageContainer()
        {
            if (sessionStorageContainer == null)
            {
                if (HttpContext.Current == null)                                    
                    sessionStorageContainer = new ThreadSessionStorageContainer();
                else
                    sessionStorageContainer = new HttpSessionContainer();
            }

            return sessionStorageContainer;
        }
    }
}
