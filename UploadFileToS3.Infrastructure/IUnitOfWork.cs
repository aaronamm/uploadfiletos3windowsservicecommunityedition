﻿using System;

namespace UploadFileToS3.Infrastructure
{
    public interface IUnitOfWork:IDisposable
    {
        void Commit(bool isWithTransactionRollback);
        void Commit();

        void Undo();
    }
}