﻿namespace UploadFileToS3.Infrastructure
{
public   interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
        IUnitOfWork Create(bool forceNew);
    }
}
