﻿using System.Collections.Generic;

namespace UploadFileToS3.Infrastructure
{
    public interface IRepository<T, K> where T: IAggregateRoot
    {
        void Add(T entity);
        void Remove(T entity);
        T FindById(K Id);
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAll(int index, int count);
    }

}
