﻿using System;

namespace UploadFileToS3.Models
{
    public interface ICreateDate
    {
        DateTime CreateDate { get; set; }
    }
}