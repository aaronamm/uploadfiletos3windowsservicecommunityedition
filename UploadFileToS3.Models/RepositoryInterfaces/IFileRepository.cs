using UploadFileToS3.Infrastructure;

namespace UploadFileToS3.Models.RepositoryInterfaces
{
    public interface IFileRepository : IRepository<UploadedFile, int>
    {
        UploadedFile FindByFileName(string fileName);
    }
}