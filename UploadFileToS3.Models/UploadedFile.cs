﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UploadFileToS3.Infrastructure;

namespace UploadFileToS3.Models
{
    public class UploadedFile: IAggregateRoot
    {
        public virtual int Id { get; set; }
        public virtual string FileName { get; set; }
        public virtual string S3FileKey { get; set; }

        public virtual DateTime StartUploadTimeUtc { get; set; }
        public virtual DateTime FinishUploadedTimeUtc { get; set; }
        public virtual long FileSize { get; set; }
    }
}
