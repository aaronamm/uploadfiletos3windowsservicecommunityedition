﻿using System;

namespace UploadFileToS3.Models
{
    public interface ILastUpdate
    {
        DateTime LastUpdate { get; set; }
    }
}