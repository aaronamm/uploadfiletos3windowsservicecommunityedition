﻿using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using FluentAssertions;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;
using UploadFileToS3.Repository.NH;
using UploadFileToS3.Repository.NH.Repositories;
using UploadFileToS3.Service;
using log4net;
using log4net.Config;

namespace UploadFileToS3.Tests.Integration
{
    [TestFixture]
    public class FileServiceTest
    {
        private ILog _log = LogManager.GetLogger("UploadFileToS3.Tests.Integration");

        private string dbPath = ConfigurationManager.AppSettings["dbPath"];

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            XmlConfigurator.Configure();
        }

        [SetUp]
        public void SetUp()
        {
            if (File.Exists(dbPath))
            {
                File.Delete(dbPath);
            }

            SQLiteConnection.CreateFile(dbPath);
            SessionFactory.Init();

            //create table
            var schemaExport = new SchemaExport(SessionFactory.Config);
            schemaExport.SetOutputFile("db_create.sql");
            schemaExport.Create(true, true);

        }

        [Test]
        public void AddFile()
        {


        }

        [Test]
        public void AddNewUploadedFile_ValidInput_1UploadedFileSave()
        {
            var unitOfWorkFactory = new NHUnitOfWorkFactory();
            var fileRepository = new FileRepository();
            var s3Service = new AmazonS3Service();

            var fileService = new FileService(unitOfWorkFactory,s3Service, fileRepository);

            fileService.UploadedFileToS3(new []{ new FileInfo(@"c:\backup\file.bak")});


            using (unitOfWorkFactory.Create())
            {
                var uploadedFiles = fileRepository.FindAll();
                _log.DebugFormat("uploadedFile count: {0}", uploadedFiles.Count());
                uploadedFiles.Count().Should().Be(1);
            }

        }


        [TearDown]
        public void TearDown()
        {


        }
    }
}