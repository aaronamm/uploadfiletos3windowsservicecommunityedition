﻿using System;
using System.IO;
using System.Linq;
using Amazon;
using NUnit.Framework;
using UploadFileToS3.Service;
using log4net;
using log4net.Config;

namespace UploadFileToS3.Tests.Integration
{

    [TestFixture]
    public class AmazonS3ServiceTest
    {

        private ILog _log = LogManager.GetLogger("UploadFileToS3.Tests.Integration");

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            XmlConfigurator.Configure();
            _log.Debug("log turn on");
        }

        [Test]
        public void UploadingLargeFile_FileExist_FileUploaded()
        {

            var amazoneS3Service = new AmazonS3Service();
            var s3Filekey = "file.bak";
            var fileName = @"file.bak";
            var description = "";
            var filePath = @"C:\backup\file.bak";

            amazoneS3Service.UploadingLargeFile(
                s3Filekey,
                fileName,
                description,
                filePath,
                Amazon.S3.S3CannedACL.PublicRead);
        }

        [Test]
        public void TestGetEndpoint()
        {

            //list all possible region
            foreach (var region in RegionEndpoint.EnumerableAllRegions)
            {
                _log.DebugFormat("systemname: {0}", region.SystemName);
            }

            var region1 = RegionEndpoint.EnumerableAllRegions.Single(r => r.SystemName == "us-east-1");
            _log.Debug(region1);

        }
    }
}