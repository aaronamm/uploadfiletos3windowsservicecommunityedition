﻿using System;
using System.Collections.Generic;
using System.IO;
using UploadFileToS3.Infrastructure;
using UploadFileToS3.Models;
using UploadFileToS3.Models.RepositoryInterfaces;
using log4net;

namespace UploadFileToS3.Service
{
    public class FileService
    {
        private ILog _log = LogManager.GetLogger("UploadFileToS3WindowsService.Service");

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly AmazonS3Service _amzoneS3Service;
        private readonly IFileRepository _fileRepository;

        public FileService(
            IUnitOfWorkFactory unitOfWorkFactory,
            AmazonS3Service amzoneS3Service,
            IFileRepository fileRepository)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _amzoneS3Service = amzoneS3Service;
            _fileRepository = fileRepository;
        }

        public bool UploadedFileToS3(FileInfo[] files)
        {
            try
            {
                //var bucketName = "noonswoon.dbbackups";
                var description = "";

                //upload
                var uploadedFiles = new List<UploadedFile>();

                foreach (var fileInfo in files)
                {
                    //check if file already uploaded
                    var existingUploadedFile = _fileRepository.FindByFileName(fileInfo.Name);
                    if (existingUploadedFile != null)
                    {
                        _log.WarnFormat("file name: {0} already uploaded", fileInfo.Name);
                        continue;
                    }

                    var s3Filekey = fileInfo.Name;
                    var startTime = DateTime.UtcNow;

                    _amzoneS3Service.UploadingLargeFile(
                        s3Filekey,
                        fileInfo.Name,
                        description,
                        fileInfo.FullName,
                        Amazon.S3.S3CannedACL.Private);
                    var finishTime = DateTime.UtcNow;

                    var uploadedFile = new UploadedFile()
                    {
                        FileName = fileInfo.Name,
                        S3FileKey = s3Filekey,
                        StartUploadTimeUtc = startTime,
                        FinishUploadedTimeUtc = finishTime,
                        FileSize = fileInfo.Length,
                    };
                    uploadedFiles.Add(uploadedFile);
                }

                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    foreach (var uploadedFile in uploadedFiles)
                    {
                        _fileRepository.Add(uploadedFile);

                    }

                    unitOfWork.Commit();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return false;
            }

        }

    }
}