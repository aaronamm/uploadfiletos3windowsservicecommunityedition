﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using log4net;

namespace UploadFileToS3.Service
{
    public class AmazonS3Service
    {
        private ILog _log = LogManager.GetLogger("UploadFileToS3WindowsService.Service");

        private Dictionary<string, int> uploadTracker = new Dictionary<string, int>();

        public string UploadingLargeFile(
            string s3FileKey,
            string fileName,
            string fileDesc,
            string fullPath,
            S3CannedACL cannedACL)
        {
            if (s3FileKey == null) throw new ArgumentNullException("s3FileKey");
            try
            {

                _log.DebugFormat("WritingLargeFile: Create TransferUtilityUploadRequest");

                //get configuration values from Config File (web.config or app.config)
                var awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
                var awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
                var bukcketName = ConfigurationManager.AppSettings["bucketName"];

                var regionEndpointSystemName = ConfigurationManager.AppSettings["bucketRegionEndpiont"];
                _log.InfoFormat("regionEndpointSystemName: {0}", regionEndpointSystemName);
                var regionEndpoint = RegionEndpoint.EnumerableAllRegions
                    .Single(r => r.SystemName == regionEndpointSystemName);
                _log.InfoFormat("regionEndpoint.SystemName: {0}", regionEndpoint.SystemName);

                var serviceUrl = ConfigurationManager.AppSettings["bucketServiceUrl"];
                var timeOutInHours = TimeSpan.FromHours(
                    Convert.ToDouble(ConfigurationManager.AppSettings["timeOutInHours"]));
                var filePartSizeInMb = Convert.ToInt64(
                    ConfigurationManager.AppSettings["filePartSizeInMb"]) * 1024 * 1024; //convert byte to MB

                var request = new TransferUtilityUploadRequest();
                request.BucketName = bukcketName;
                request.Key = s3FileKey;
                request.Metadata.Add("fileName", fileDesc);
                request.Metadata.Add("fileDesc", fileDesc);
                request.CannedACL = cannedACL;
                request.FilePath = fullPath;
                request.Timeout = timeOutInHours;
                request.PartSize = filePartSizeInMb;
                    

                _log.Debug("WritingLargeFile: Create TransferUtility");



                request.UploadProgressEvent += UploadRequestUploadPartProgressEvent;
                var s3Config = new AmazonS3Config()
                {
                    RegionEndpoint = regionEndpoint, //Amazon.RegionEndpoint.USEast1
                    ServiceURL = serviceUrl,//"s3.amazonaws.com",
                     
                };


                var s3Client = new AmazonS3Client(awsAccessKey, awsSecretKey, s3Config);

                var fileTransferUtility = new TransferUtility(s3Client);
                _log.Debug("WritingLargeFile: Start Upload");
                fileTransferUtility.Upload(request);

                return s3FileKey;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    _log.Error("Please check the provided AWS Credentials.");
                }
                else
                {
                    _log.Error(amazonS3Exception);
                }
                return String.Empty; //Failed
            }
        }

        public int GetAmazonUploadPercentDone(string fileName)
        {
            if (!uploadTracker.ContainsKey(fileName))
                return 0;
            return uploadTracker[fileName];
        }

        private void UploadRequestUploadPartProgressEvent(object sender, UploadProgressArgs e)
        {
            var req = sender as TransferUtilityUploadRequest;
            if (req != null)
            {
                var fileName = req.FilePath.Split('\\').Last();
                if (!uploadTracker.ContainsKey(fileName))
                    uploadTracker.Add(fileName, e.PercentDone);

                //When percentage done changes add logentry:
                if (uploadTracker[fileName] != e.PercentDone)
                {
                    uploadTracker[fileName] = e.PercentDone;
                    _log.DebugFormat("WritingLargeFile progress: {1} of {2} ({3}%) for file '{0}'",
                        fileName, e.TransferredBytes, e.TotalBytes, e.PercentDone);
                }
            }

        }

    }
}
