﻿using System;

namespace UploadFileToS3.Repository.NH
{
    public class UniqueKeyException : Exception
    {
        public UniqueKeyException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }

}
