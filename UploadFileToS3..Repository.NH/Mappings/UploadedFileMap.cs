﻿using FluentNHibernate.Mapping;
using UploadFileToS3.Models;

namespace UploadFileToS3.Repository.NH.Mappings
{
    public class UploadedFileMap:ClassMap<UploadedFile>
    {
        public UploadedFileMap()
        {
            Id(x => x.Id);
            Map(x => x.FileName).UniqueKey("UX_UploadedFile_FileName");
            Map(x => x.FileSize);

            Map(x => x.S3FileKey);
            Map(x => x.StartUploadTimeUtc);
            Map(x => x.FinishUploadedTimeUtc);


        }
    }
}