﻿using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Event;
using UploadFileToS3.Infrastructure.SessionStorage;
using UploadFileToS3.Repository.NH.Mappings;
using Configuration = NHibernate.Cfg.Configuration;

namespace UploadFileToS3.Repository.NH
{
    public class SessionFactory
    {
        private static ISessionFactory sessionFactory;
        private static Configuration config;

        public static Configuration Config
        {
            get { return config; }
        }

        public static void Init()
        {
            config = Fluently.Configure()
                    .Database(GetDatabaseConfig())
                    .Mappings(m =>
                                  {
                                      m.FluentMappings.AddFromAssemblyOf<UploadedFileMap>();
                                      //to use name query in xml file
                                      m.HbmMappings.AddFromAssemblyOf<UploadedFileMap>();
                                  })
                    .ExposeConfiguration(TreatConfiguration)
                    .BuildConfiguration(); 

            sessionFactory = config.BuildSessionFactory();
        }

        private static void TreatConfiguration(Configuration cfg)
        {
            //config.SetProperty("current_session_context_class", "web");
            cfg.EventListeners.PreInsertEventListeners
            = new IPreInsertEventListener[]
                                                 {
                                                    new AuditEventListener()
                                                 };

            cfg.EventListeners.PreUpdateEventListeners

                 = new IPreUpdateEventListener[]
                                                 {
                                                    new AuditEventListener()
                                                 };

            //for unique constrain exception
            cfg.SetProperty("sql_exception_converter",
                typeof(SqlServerExceptionConverter).AssemblyQualifiedName);
        }

        private static IPersistenceConfigurer GetDatabaseConfig()
        {
            var dbPath = ConfigurationManager.AppSettings["dbPath"];
            var databaseConfig = SQLiteConfiguration.Standard.UsingFile(dbPath);
            return databaseConfig;
        }

        public static ISessionFactory GetNHSessionFactory()
        {
            if (sessionFactory == null)
                Init();//we can move this to Global.ascx
            return sessionFactory;
        }

        private static ISession GetNewSession()
        {
            return GetNHSessionFactory().OpenSession();
        }

        public static NHibernate.ISession GetCurrentSession()
        {
            var sessionStorageContainer = SessionStorageFactory.GetStorageContainer();

            var currentSession = sessionStorageContainer.GetCurrentSession();
            if (currentSession == null)
            {
                currentSession = GetNewSession();
                sessionStorageContainer.Store(currentSession);
            }
            return currentSession;
        }

        public static void ClearCurrentSession()
        {
            var sessionStorageContainer = 
                SessionStorageFactory .GetStorageContainer();
            sessionStorageContainer.Clear();

        }

    }//end class
}
