﻿using System.Linq;
using NHibernate.Linq;
using UploadFileToS3.Models;
using UploadFileToS3.Models.RepositoryInterfaces;

namespace UploadFileToS3.Repository.NH.Repositories
{
    public class FileRepository : RepositoryBase<UploadedFile, int>, IFileRepository
    {

        public UploadedFile FindByFileName(string fileName)
        {
            var session = SessionFactory.GetCurrentSession();
            return session.Query<UploadedFile>().SingleOrDefault(f => f.FileName == fileName);
        }
    }
}
