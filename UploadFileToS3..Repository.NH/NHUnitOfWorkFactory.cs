﻿using UploadFileToS3.Infrastructure;

namespace UploadFileToS3.Repository.NH
{
    public class NHUnitOfWorkFactory : IUnitOfWorkFactory
    {
        public IUnitOfWork Create()
        {
            return Create(false);
        }

        public IUnitOfWork Create(bool forceNew)
        {
            return new NHUnitOfWork(forceNew);
        }
    }
}
