﻿using System;
using NHibernate.Event;
using NHibernate.Persister.Entity;
using UploadFileToS3.Models;
using log4net;

namespace UploadFileToS3.Repository.NH
{
    /// <summary>
    /// help to set CreateDate and LastUpdate Value for Model Class
    /// </summary>
    public class AuditEventListener : IPreUpdateEventListener, IPreInsertEventListener
    {
        private ILog _log = LogManager.GetLogger("AuditEventListener");

        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            var audit = @event.Entity as ILastUpdate;
            if (audit == null)
                return false;

            var utcNow = DateTime.UtcNow;
            //log.DebugFormat("localNow = {0}, audit type {1}", localNow, audit.GetType());            

            Set(@event.Persister, @event.State, "LastUpdate",utcNow);
            audit.LastUpdate =utcNow;
            return false;
        }
       
        public bool OnPreInsert(PreInsertEvent @event)
        {
            var audit = @event.Entity as ICreateDate;
            if (audit == null)
                return false;

            var utcNow = DateTime.UtcNow;
            //log.DebugFormat("localNow = {0}, audit type {1}", localNow, audit.GetType());            

            Set(@event.Persister, @event.State, "CreateDate",utcNow);
            audit.CreateDate =utcNow;
            return false;
        }

        private void Set(IEntityPersister persister, object[] state, string propertyName, object value)
        {
            var index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index == -1)
                return;
            state[index] = value;
        }

    }
}
