﻿using UploadFileToS3.Infrastructure;

namespace UploadFileToS3.Repository.NH
{
    /// <summary>
    /// create in new thread for console app
    /// </summary>
    public class NHUnitOfWork: IUnitOfWork
    {

        public NHUnitOfWork(bool forceNewContext)
        {
            if (forceNewContext)
            {
                SessionFactory.ClearCurrentSession();
            }
        }

        //public void RegisterAmended(IAggregateRoot entity)
        //{
        //    SessionFactory.GetCurrentSession().SaveOrUpdate(entity);
        //}

        //public void RegisterNew(IAggregateRoot entity)
        //{
        //    SessionFactory.GetCurrentSession().Save(entity);
        //}

        //public void RegisterRemoved(IAggregateRoot entity)
        //{
        //    SessionFactory.GetCurrentSession().Delete(entity);
        //}

        public void Commit(bool isWithTransactionRollback)
        {
            //without transaction
            if (!isWithTransactionRollback)
            {
                SessionFactory.GetCurrentSession().Flush();
                return;
            }

            using (var tx = SessionFactory.GetCurrentSession().BeginTransaction())
            {
                try
                {
                    tx.Commit();
                }
                catch 
                {
                    if( tx.IsActive) tx.Rollback();
                    throw;
                }
            }
        }

        public void Commit()
        {
            Commit(false);
        }

        public void Undo()
        {
            SessionFactory.ClearCurrentSession();
        }

        public void Dispose()
        {
            SessionFactory.ClearCurrentSession();
        }

    }//end class
}