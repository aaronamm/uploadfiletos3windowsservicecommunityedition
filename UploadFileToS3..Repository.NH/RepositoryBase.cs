﻿using System.Collections.Generic;
using NHibernate;
using UploadFileToS3.Infrastructure;
using log4net;

namespace UploadFileToS3.Repository.NH
{    
    public abstract class RepositoryBase<T, K>  :IRepository<T,K> where T: IAggregateRoot
    {
        protected ILog _log = LogManager.GetLogger("Noonswoon.Repository");

        public void Add(T entity)
        {
            SessionFactory.GetCurrentSession().Save(entity);
        }

        public void Remove(T entity)
        {
            SessionFactory.GetCurrentSession().Delete(entity);
        }


        public T FindById(K id)
        {

            return SessionFactory.GetCurrentSession().Get<T>(id);
        }

        public IEnumerable<T> FindAll()
        {
            ICriteria CriteriaQuery = 
                SessionFactory.GetCurrentSession().CreateCriteria(typeof(T));

            return CriteriaQuery.List<T>();
        }

        public IEnumerable<T> FindAll(int index, int count)
        {
            ICriteria CriteriaQuery = 
                SessionFactory.GetCurrentSession().CreateCriteria(typeof(T));

            return CriteriaQuery
                .SetFetchSize(count)
                .SetFirstResult(index)
                .List<T>();
        }

    }
}
