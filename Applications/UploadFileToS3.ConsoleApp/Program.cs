﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using UploadFileToS3.Repository.NH;
using UploadFileToS3.Repository.NH.Repositories;
using log4net;
using log4net.Config;

namespace UploadFileToS3.ConsoleApp
{
    public class Program
    {

        private static ILog _log = LogManager.GetLogger("UploadFileToS3.ConsoleApp");

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            _log.Debug("log turn on");

            //create NHibernate session factory
            SessionFactory.Init();

            //start job
            JobConfig.Config();


            Console.ReadLine();

        }
    }
}
