﻿namespace UploadFileToS3.WindowsService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UploadFileToS3ServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.UploadFileToS3ServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // UploadFileToS3ServiceProcessInstaller
            // 
            this.UploadFileToS3ServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.UploadFileToS3ServiceProcessInstaller.Password = null;
            this.UploadFileToS3ServiceProcessInstaller.Username = null;
            // 
            // UploadFileToS3ServiceInstaller
            // 
            this.UploadFileToS3ServiceInstaller.Description = "Uploade File To S3 with cron job that can be config in App.config";
            this.UploadFileToS3ServiceInstaller.DisplayName = "UploadFileToS3WindowsService";
            this.UploadFileToS3ServiceInstaller.ServiceName = "UploadFileToS3WindowsService";
            this.UploadFileToS3ServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.UploadFileToS3ServiceProcessInstaller,
            this.UploadFileToS3ServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller UploadFileToS3ServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller UploadFileToS3ServiceInstaller;
    }
}