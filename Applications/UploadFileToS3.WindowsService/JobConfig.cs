﻿using System;
using System.Configuration;
using Quartz;
using Quartz.Impl;

namespace UploadFileToS3.WindowsService
{
    public class JobConfig
    {

        public static void Config()
        {
            // construct a scheduler
            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();
            scheduler.Start();

            var updateConfigJobForWeb = JobBuilder.Create<UploadFileJob>().Build();
            scheduler.ScheduleJob(updateConfigJobForWeb, GetUploadFileJobTrigger());
           
        }


        private static ITrigger GetUploadFileJobTrigger()
        {
            var cronSchedule = ConfigurationManager.AppSettings["uploadSchedule"];
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time"); //Bangkok
            var trigger = TriggerBuilder.Create()
                .WithIdentity("uploadFileTrigger")
                .WithCronSchedule(cronSchedule, x => x.InTimeZone(timeZoneInfo))
                .Build();

            return trigger;

        }

    }
}
