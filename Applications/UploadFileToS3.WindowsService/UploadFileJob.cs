﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Quartz;
using UploadFileToS3.Repository.NH;
using UploadFileToS3.Repository.NH.Repositories;
using UploadFileToS3.Service;
using log4net;

namespace UploadFileToS3.WindowsService
{
    public class UploadFileJob : IJob
    {

        private static ILog _log = LogManager.GetLogger("UploadFileToS3WindowsService.Service");

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("UploadFileJob get started");

            var filePath = ConfigurationManager.AppSettings["fileDirectory"];
            var fileExtension = ConfigurationManager.AppSettings["fileExtension"];//"*.pdf";

            var allFiles = Directory.GetFiles(filePath, fileExtension, SearchOption.AllDirectories);
            _log.DebugFormat("allFiles found count: {0}", allFiles.Count());

            if (!allFiles.Any()) return;

            var unitOfWorkFactory = new NHUnitOfWorkFactory();
            var fileRepository = new FileRepository();
            var s3Service = new AmazonS3Service();

            var fileService = new FileService(unitOfWorkFactory, s3Service, fileRepository);
            //get the last modifile
            var latestFile = allFiles.Select(f => new FileInfo(f)).OrderByDescending(f => f.LastWriteTimeUtc).FirstOrDefault();
            if (latestFile == null)
            {
                _log.Warn("no lalest file to upload");
                return;
            }

            fileService.UploadedFileToS3(new []{latestFile});

            _log.InfoFormat("done at UTC: {0}", DateTime.UtcNow);
        }
    }
}
