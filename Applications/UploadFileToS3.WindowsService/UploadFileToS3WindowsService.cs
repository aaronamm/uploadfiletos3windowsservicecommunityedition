﻿using System;
using System.ServiceProcess;
using UploadFileToS3.Repository.NH;
using log4net;
using log4net.Config;

namespace UploadFileToS3.WindowsService
{
    public partial class UploadFileToS3WindowsService : ServiceBase
    {

        private static ILog _log = LogManager.GetLogger("UploadFileToS3WindowsService");

        public UploadFileToS3WindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            XmlConfigurator.Configure();
            _log.Debug("log turn on");


            _log.InfoFormat("service start at UTC: {0}", DateTime.UtcNow);

            //create NHibernate session factory
            SessionFactory.Init();

            //start job 
            JobConfig.Config();
        }

        protected override void OnStop()
        {
            _log.InfoFormat("service stop at UTC: {0}", DateTime.UtcNow);
        }
    }
}
