﻿using System.ServiceProcess;

namespace UploadFileToS3.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new UploadFileToS3WindowsService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
